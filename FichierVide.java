
/**
 * Une classe pour lever une exception levée lorsque le fichier de répertoire est vide 
 *
 * @author (Laurent Pierron)
 * @version (un numéro de version ou une date)
 */
public class FichierVide extends Exception
{
    // nom du fichier
    private final String nomFichier;

    /**
     * Constructeur d'objets de classe ChampVide
     * 
     * @param nomFichier le nom du fichier vide
     */
    public FichierVide(String nomFichier) {
        super(nomFichier);
        this.nomFichier = nomFichier;
    }

    public String toString() {
        return "Exception fichier vide : " + nomFichier;
    }
}

