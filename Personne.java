import java.io.*;

/**
 * Décrivez votre classe Personne ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Personne implements Serializable
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private final String nom;
    private final String prénom;
    private final String téléphone;

    /**
     * Constructeur d'objets de classe Personne
     */
    public Personne(String nom, String prénom, String téléphone)
    {
        // initialisation des variables d'instance
        this.nom = nom;
        this.prénom = prénom;
        this.téléphone = téléphone;
    }

    public String toString() {
        return nom + ":" + prénom + ":" + téléphone;
    }
}
